<?php
require 'rec_data.php';
// Cette constante permet de stocker le nombre de lignes que l'on veut afficher par page;
define('PER_PAGE', 4);

//$query  = 'SELECT * FROM products';
$queryCount = 'SELECT COUNT(id) as count FROM products';

//cette ligne permet, quand on clique sur le bouton: Page suivante, que la page affiche la page suivante
$page = (int)($_GET['p'] ?? 1);

//Cette variable offset permet d'afficher les élément des pages suivantes avec leur numéro de lignes

$offset = ($page-1) * PER_PAGE;

$query .= " LIMIT " . PER_PAGE . "OFFSET $offset";

$statement = $pdo->prepare($queryCount);
$statement->execute();
$count =  (int)$statement->fetch()['count'];
$pages = ceil($count / PER_PAGE);

//var_dump($pages);