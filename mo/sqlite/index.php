<?php
use App\NumberHelper;
use App\URLHelper;
use App\TableHelper;

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="../favicon.ico" />
    <title>Biens Immobiliers</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <script src="https://use.fontawesome.com/0b5fa10a1b.js"></script>

    <link rel="stylesheet" href="css/style.css">
</head>

<?php

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.
('manudon.com' === $_SERVER['HTTP_HOST'] ?
'' : '..').'/gc7/tools/vd.php';

require_once 'cnx.php';
require 'rec_data.php';
require 'src/NumberHelper.php';
require 'src/URLHelper.php';
require 'src/TableHelper.php';
?>

<body class="p-3">
    <?php include '../../config/home.php' ?>
    <?php
    /*vd($ch1, $monTablo, $monObjet);
    svd('ch1', 'monTablo', 'monObjet');
    pr($ch1, $monTablo, $monObjet);
    spr('ch1', 'monTablo', 'monObjet');*/
    ?>
    <h1>Page <?php echo $page; ?></h1>
    <h2>Mes biens Immobiliers</h2>


    <form action="<?=$_SERVER['PHP_SELF']?>" method="GET" class="mb-4">
        <div class="form-group">
            <input type="text" class="form-control" name="q" placeholder="Rechercher par ville"
                value="<?php echo htmlentities($_GET['q'] ?? null); ?>">
        </div>
        <button class="btn btn-primary">Rechercher </button>
    </form>


    <table class="table table-striped">
        <thead>
            <tr>
                <?php //2doMO Add class B4 pour no-wrap ?>
                <th><?= TableHelper::sort('id', 'ID', $_GET) ?></th>
                <th><?= TableHelper::sort('name', 'Nom', $_GET) ?></th>
                <th><?= TableHelper::sort('price', 'Prix', $_GET) ?></th>
                <th><?= TableHelper::sort('city', 'Ville', $_GET) ?></th>
                <th><?= TableHelper::sort('adress', 'Addresse', $_GET) ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product) { ?>
            <tr>
                <!-- //@not Alignement à droite pour cette colonne -->
                <td class="text-right">#<?php echo $product['id']; ?></td>
                <td><?php echo $product['name']; ?></td>
                <?php //2doMO Add class B4 pour no-wrap ?>
                <td><?php echo NumberHelper::price($product['price']); ?></td>
                <td><?php echo $product['city']; ?></td>
                <td><?php echo $product['address']; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>


    <?php if ($pages > 1 && $page > 1) { ?>
    <a href=" ?<?php echo URLHelper::withParam($_GET,'p', $page - 1); ?>" class="btn btn-primary">Page précédente</a>
    <?php } ?>

    <?php if ($pages > 1 && $page < $pages) { ?>
    <a href="?<?php echo URLHelper::withParam($_GET,'p', $page + 1); ?>" class="btn btn-primary">Page
        suivante</a>
    <?php } ?>


</body>

</html>