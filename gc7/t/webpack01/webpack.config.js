const webpack = require("webpack");
const path = require("path");

let config = {
  entry: "./src/index.js",
  mode : "development",
  watch: true,
  watchOptions: {
    poll: 1000 // Check for changes every second
  },
  output: {
    path: path.resolve(__dirname, "./public"),
    filename: "./bundle.js"
  }
};

module.exports = config;
