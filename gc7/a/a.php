<?php

namespace GC7;

include '../tools/partials/fonctions.php';
//? require __DIR__.'/vendor/autoload.php';
// use Bolt\Common\Str;

// [ Série => ['URL1', 'URL2',] ] - Le titre de la page est le nom du fichier
$pages = [
    'tests' => [
        'tutos/divers/diapVie_LesVacancesSos',
        'tutos/divers/diapos',
        'tutos/divers/uuu',
        'tutos/divers/diapos',
        'tutos/divers/photos',
    ],

    'tests2' => [
        'tutos/divers/dia_pos2',
        'tutos/divers/phoTos2',
        'tutos/divers/phoTos2',
    ],

    'tests3' => [
        'tutos/divers/diaPos3',
        'tutos/divers/lesBellesVacances',
    ],
    'testsa' => [
        'tutos/divers/diapos',
        'tutos/divers/photos',
    ],

    'tests2b' => [
        'tutos/divers/dia_pos2',
        'tutos/divers/phoTos2',
    ],

    'tests2bb' => [
        'tutos/divers/dia_pos2',
        'tutos/divers/phoTos2',
    ],

    'tests3c' => [
        'tutos/divers/diaPos3',
        'tutos/divers/lesBellesVacances',
    ],
];

/** @v svd('pages'); */
$cols = 'col-xl-2 col-lg-3 col-md-4 col-sm-6 my-2';
?>
<h1 class="text-center">Test</h1>
<div class=" row justify-content-center">
    <?php
    foreach ($pages as $k => $page) {
        ?>
    <div class="<?php echo $cols; ?>">

        <div class="card">
            <div class="card-header text-center letspace">
                <?php echo strtoupper($k); ?>
            </div>
            <ul class="list-group list-group-flush">
                <?php
                        // svd('rubrique');

                        foreach ($page as $lien) {
                            $titre = $humanize(titre($lien)); ?>

                <?php echo lk($titre, $lien); ?>

                <?php
                            // svd('titre', 'lien');
                        } ?>
            </ul>
        </div>
    </div>
    <?php
    }
    ?>
</div>
<?php
// Pour voir svd décommenter les 2 lignes suivantes le cas échéant
// include_once 'tools/svd.php';
// spr('var1', 'var2', 'var3', 'var4', 'var5', 'var6', 'var7');

// Suite...

/**
 * Retourne le code HTML pour l'affichage dans une liste groupée selon bootstrap.
 *
 * On appelle ce type de fonction un helper
 *
 * @param string $sujet
 * @param string $lien
 * @param string [$ext] true pour nouvelle fenêtre
 */
function lk(string $titre, string $lien, bool $ext = null): string
{
    return '<li class="list-group-item p-1 px-3"><a href="p.php?page='.$lien.'" target="'.($ext).'" class="card-link">'.$titre.'</a></li>';
}

/*
<!--
<script>
$(() => {
    $('a').click((e) => {
        // e.preventDefault()
        // var form = this.value;
        $(form).submit()
        // console.log(e);
    })
    // $('[data-toggle="tooltip"]').tooltip()
})
</script>
-->
*/