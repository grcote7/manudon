<?php

/*
 * (c) Manudon - 2019
 */

ini_set('display_errors', '1');
// (c) Manudon - 2019

// include __DIR__.'/../../config/path.php';
include_once '../../config/path.php';

include_once $path.'tools/vd.php';
// svd('path');
// svd('_GET');

$page = function ($page) {
    $titre = titre($page);
    include '../assets/header.php';
    include $page.'.php';
    include '../assets/footer.php';
};

$page('../'.$_GET['page']);