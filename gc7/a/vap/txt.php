<?php

/*
 * (c) Manudon - 2019
 */

$txt = <<<'EOT'

Bonjour Mme Peetermans,




Conformément à notre dernier échange et afin de pouvoir procéder ensemble, à aider concrètement, autant de famille le necessitant que possible dans les mois et années à venir, bous avons le plaisir de vous adresser ci-joint le document de référence de notre asbl Vivons pour Aider des Personnes, à savoir, le plus important, ses statuts officiels déposés près le Greffe du Tribunal de Commerce de Charleroi.


Concernant la date de notre rencontre, elle sera à votre convenance, considérant que vous devez concilier 2 agendas, et de préférence pour nous cette fois, dans le début de semaine (L'idéal étant entre le lundi et le mercredi).


Dans l'attente de votre proposition de date, nous vous prions d'aggréer, Madame Peetermans, l'expession de nos salutations distinguées.


L'équipe VAP,




M. Lionel CÔTE
Président

EOT;