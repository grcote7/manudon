<?php

$humanize=function (string $text): string
{
    return ucfirst(trim(mb_strtolower(preg_replace(['/([A-Z])/', '/[_\s]+/'], ['_$1', ' '], $text))));
};


$str = 'JelAiAppellé_comme_çaParceQueCa_traiteLesChaines';
$str2 = $humanize($str);
// Donc là on suppose que le fichier d'appelle:
echo $str.'.php';

echo '<hr>Après traitement: '.$str2;

// Comme titre ds la page d'accueil, ce sera déjà mieux ! ;-)
