<?php
svd('_POST');
require '../tools/functions.php';

$parfums = [
    'Fraise'   => 4,
    'Chocolat' => 5,
    'Vanille'  => 3,
];

$cornets = [
    'Pot'    => 2,
    'Cornet' => 3,
];

$supplements = [
    'Pépites de chocolat' => 1,
    'Chantilly'           => .5,
];

// Pour avoir comme si issu du POST
/*
$_POST = [
    'parfum' => [
        'Chocolat',
        'Vanille',
    ],

    'cornet' => 'Pot',

    'supplement' => ['Chantilly'],
];
*/

$ingredients = [];
$total       = 0;
// vd($_POST);

foreach (['parfum', 'supplement', 'cornet'] as $name) {
    if (isset($_POST[$name])) {
        $liste = $name.'s';
        $choix = $_POST[$name];
        // vd($choix);
        if (is_array($choix)) {
            foreach ($choix as $value) {
                // echo 'AAA '.$liste.' '.$value;
                if (isset(${$liste}[$value])) {
                    $ingredients[] = $value;
                    $total += ${$liste}[$value];
                }
            }
        } else {
            // echo '*** '.$choix.' '.$name. ' '.$liste.' ';
            if (isset(${$liste}[$choix])) {
                $ingredients[] = $choix;
                $total += ${$liste}[$choix];
            }
        }
    }
}
?>

<h2>Composez votre glace</h2>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <h5 class="card-head p-3">Votre glace</h5>
            <div class="card-body pt-0">
                <ul>
                    <?php foreach ($ingredients as $ingredient) { ?>
                    <li><?php echo $ingredient; ?></li>
                    <?php } ?>
                </ul>
                Prix : <?php echo $total; ?> €
            </div>
        </div>
    </div>
</div>
</div>
<!-- <?php echo $_SERVER['PHP_SELF']; ?> -->
<form action="p.php?page=tutos/glace" method="POST">
    <div class="form-group">

        <h2>Parfum(s) ?</h2>
        <?php foreach ($parfums as $parfum => $prix) { ?>
        <div class=" checkbox">
            <label>
                <?php echo checkbox('parfum', $parfum, $_POST); ?>
                <?php echo $parfum; ?> - <?php echo $prix; ?> €
            </label>
        </div>
        <?php } ?>

        <hr>

        <h2>Cornet ou Pot ?</h2>
        <?php foreach ($cornets as $cornet => $prix) { ?>
        <div class="checkbox">
            <label>
                <?php echo radio('cornet', $cornet, $_POST); ?>
                <?php echo $cornet; ?> - <?php echo $prix; ?> €
            </label>
        </div>
        <?php } ?>

        <h2>Supplément(s) ?</h2>
        <?php foreach ($supplements as $supplement => $prix) { ?>
        <div class="checkbox">
            <label>
                <?php echo checkbox('supplement', $supplement, $_POST); ?>
                <?php echo $supplement; ?> - <?php echo $prix; ?> €
            </label>
        </div>
        <?php } ?>

        <button type="submit" class="btn btn-primary btn-sm my-3">
            Composer ma glace
        </button>

    </div>
</form>
