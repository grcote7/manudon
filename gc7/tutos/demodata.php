<?php

/*
 * (c) Manudon - 2019
 */

define('JOURS', [
    'Lundi',
    'Mardi',
    'Mercredi',
    'Jeudi',
    'Vendredi',
    'Samedi',
    'Dimanche',
]);

define('CRENEAUX', [
    [
        [8, 12],
        [14, 19],
    ],
    [
        [8, 12],
        [14, 19],
    ],
    [
        [8, 12],
    ],
    [
        [8, 12],
        [14, 19],
    ],
    [
        [8, 12],
        [14, 17],
    ],
    [],
    [],
]);

/**
 * Génère code HTML des horaires à partir d'un arr.
 */
function creneaux_html(array $creneaux): string
{
    if (empty($creneaux)) {
        return 'Fermé';
    }
    $phrases = [];

    foreach ($creneaux as $creneau) {
        $phrases[] = 'de <strong>'.$creneau[0].'h</strong> à <strong>'.$creneau[1].'h</strong>';
    }

    return 'Ouvert '.implode(' et ', $phrases);
}