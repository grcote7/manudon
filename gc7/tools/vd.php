<?php
ini_set('display_errors', '1');

//@q IMPORTANT:
//@v Ajouter à votre SCSS:
//@v @import '../../gc7/tools/vd';
//@s En adaptant le chemin au besoin

// declare(strict_types=1);
ini_set('xdebug.overload_var_dump', '1');
ini_set('xdebug.var_display_max_depth', -1); // Mets le max de profondeurs possibles
?>
<?php
function vdgc7(...$var): void // Super vd/pr 1er param:               vd si 0 - pr si 1
{
    echo (!$var[0]) ? varDumpToString($var[1]) : print_rToString($var[1]);
}

function get_caller_info()
{
    $c     = '';
    $file  = '';
    $func  = '';
    $class = '';
    $trace = debug_backtrace();

    // vd($trace);

    if (isset($trace[2])) {
        $file = $trace[1]['file'];
        $line = $trace[1]['line'];
        $func = $trace[2]['function'];

        if (('include' === substr($func, 0, 7)) || ('require' === substr($func, 0, 7))) {
            $func = '';
        }
    } elseif (isset($trace[1])) {
        $file = $trace[1]['file'];
        $line = $trace[1]['line'];
        $func = '';
    }

    if (isset($trace[3]['class'])) {
        $class = $trace[3]['class'];
        $func  = $trace[3]['function'];
        $file  = $trace[2]['file'];
        $line  = $trace[2]['line'];
    } elseif (isset($trace[2]['class'])) {
        $class = $trace[2]['class'];
        $func  = $trace[2]['function'];
        $file  = $trace[1]['file'];
        $line  = $trace[1]['line'];
    }

    // if ($file != '') $file = basename($file);
    if ('' !== $file) {
        $file = ($file);
    }

    $dmn  = pathinfo(dirname(__DIR__))['basename'];
    $path = dirname($dmn);
    // var_dump($dmn, $path, pathinfo($file), pathinfo(dirname($file))['basename']);
    // $file = $_SERVER['PHP_SELF'];
    $pathes = explode('/', $file);

    // var_dump($pathes);
    for ($i = 0; $i < 6; ++$i) {
        array_shift($pathes);
    }

    $file = implode('/', $pathes);
    $c    = '['.$dmn.'] '.$file.' ('.$line.')';
    $c .= ('' !== $class) ? ':'.$class.'->' : '';
    $c .= ('' !== $func) ? $func.'(): ' : '';

    return '<span style="color:blue;">'.$c.'</span>';
}

function vd(...$vars)
{
    echo '<fieldset class="svd"><legend>'.get_caller_info().' <small><i>vd()</i></small></legend><pre><code>';

    $c = count($vars) - 1;

    foreach ($vars as $k => $var) {
        $affK = ($c < 1) ? '' : ($k + 1).') ';
        echo '<span class="mev">'.$affK.' </span><code>'.varDumpToString($var).'</code>';
    }
    echo '</code></pre></fieldset>';
}

function varDumpToString($var)
{
    ob_start();
    ini_set('xdebug.overload_var_dump', '1');
    echo var_dump($var);
    ini_set('xdebug.overload_var_dump', '2');

    return ob_get_clean();
}

function print_rToString($var)
{
    ob_start();
    ini_set('xdebug.overload_var_dump', '1');
    // echo '<pre class="xdebug-var-dump">';
    print_r($var);
    // echo '</pre><br>';
    ini_set('xdebug.overload_var_dump', '2');

    return ob_get_clean();
}

function svd(...$vars)
{
    echo '<fieldset class="svd"><legend>'.get_caller_info().' - <small><i>svd()</i></small></legend><pre><code>';

    foreach ($vars as $var) {
        global ${$var};
        echo '<span class="mev">$'.$var.'</span> = ';

        vdgc7(0, ${$var});
    }

    echo '</code></pre></fieldset>';
}

function pr(...$vars)
{
    echo '<fieldset class="svd"><legend>'.get_caller_info().' - <small><i>pr()</i></small></legend><pre><code>';

    foreach ($vars as $k => $var) {
        echo '<span class="pr">'.($k + 1).') ';
        print_r($var);
        echo '</span>';
    }

    echo '</code></pre></fieldset>';
}

function spr(...$vars)
{
    echo '<fieldset class="svd"><legend>'.get_caller_info().' - <small><i>spr()</i></small></legend><pre><code>';

    foreach ($vars as $var) {
        global ${$var};
        echo '<span class="mev">$'.$var.'</span> = ';

        vdgc7(1, ${$var});
    }

    echo '</code></pre></fieldset>';
}

// $a = [1, 2, 'c'=>'d'];
// svd('a', 'lio');

function titre($page)
{
    $path = explode('/', $page);
    // vd(end($path));

    return ucfirst(end($path));
}