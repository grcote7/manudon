<style>
p,
li {
    text-align: justify;
}

a,
a:hover {
    text-decoration: none;
}


mark {
    background-color: yellow
}



article {
    font-size: 1.5em;
}
</style>
<?php // (c) Manudon - 2019

$maClosure = function () {
    return 'Je ne fais pas grand chôse mais j\'existe pour être reconnue pas svd()';
}

;

$maClasse             = new class() {
    public $propriete = 'Une caractéristique';

    public function affVar()
    {
        echo $this->var;
    }
};

$var1 = null;
$var2 = '';
$var3 = 123456;
$var4 = 'Super var_dump() By GC7 &copy; 2019';
$var5 = [1,
    2,
    ['a',
        'b',
        ['info1' => 'Salut',
            'Oki21', ], ], ];
$var6 = $maClosure;
$var7 = $maClasse;

$style = 'class="code muted small border rounded bg-dark text-light text-center p-1 mt-3"';
?><h1>Test de vd(),
    svd(),
    pr() & spr()</h1>
<p class="h5">4 fonctions "maison"(1er jet) pour aider dans le débogage</p>
</p>
<ul>
    <li><a href="#var-dump">var_dump()</a>: <a href="https://www.php.net/manual/en/function.var-dump.php"
            target="_blank">Fonction native de PHP <i aria-hidden="true" class="fa fa-external-link"></i></a></li>
    <li><a href="#vd">vd()</a>: Comme var-dump,
        se param pareil,
        donne pareil;
        Juste présentation différente que l'on peut
        personnaliser </li>
    <li><a href="#svd">svd()</a> "S, V, D" , comme Super Var_Dump. Différence majeure avec vd(): Cette fonction accepte
        comme paramètre, autant que l'on en veut, non pas une variable, mais le nom de celle-ci en chaîne de caractères.
        <h4>En quoi
            celle-ci est différente ? </h4>
        Souvent,
        il est difficile de retrouver des vd() qui traînent par ci,
        par là... Cette fonction <strong>affiche en gros l'endroit où elle a été écrite.</strong><br>En conéquence
        et intérêt, la sortie indique de quelle variable il s'agît (Et ce, quelle que soit sont contenu (scalaire,
        tableau, closure ou objet) !
        <br>
    </li>
    <li><a href="#print_r">print_r</a>: <a href="https://www.php.net/manual/fr/function.print-r.php"
            target="_blank">Fonction native de PHP <i aria-hidden="true" class="fa fa-external-link"></i></a></li>
    <li><a href="#pr">pr()</a>: Il est à print_r() ce qu'est vd() à var_dump() ! </li>
    <div class="row w-100 my-3 w-sd-50 ">
        <div class="col-12 col-md-8 mx-auto">
            <img src="tools/spr.png" class="img-fluid mx-auto" alt="Capture ecran pour spr()">
        </div>
    </div>
    <li><a href="#spr">spr()</a> <mark>"S, P, R" , comme Super Print_R: Le préféré de beaucoup! Il allie clareté et
            précisions</mark>
    </li>
</ul>
</h4>


<fieldset>
    <h4>Liste des variables pour le test </h4>
    <pre><code>$var1=null;
$var2='';
$var3=123456;
$var4='Super var_dump() By GC7 &copy; 2019';
$var5=[1,2,['a','b',['info1'=>'Salut','Oki21']]];
$var6=$maClosure; // Une simple closure
$var7=$maClasse; // Une classe minimaliste
</code></pre>
</fieldset>



<fieldset><?php var_dump($var1, $var2, $var3, $var4, $var5, $var6, $var7);
?><legend style="border:none; background-color:rgba(0,0,0,0);">
        <h2 <?php echo $style;
?>id="var-dump">var_dump($var1, $var2, $var3, $var4, $var5, $var6,
            $var7);
        </h2>
    </legend>
</fieldset>



<article class="pt-3 mt-3" id="vd">
    <h2 <?php echo $style;
?>>vd($var1, $var2, $var3, $var4, $var5, $var6, $var7);
    </h2>
</article>
<?php vd($var1, $var2, $var3, $var4, $var5, $var6, $var7); ?>


<article class="pt-3 mt-3" id="svd">
    <h2 <?php echo $style;
?>>svd('var1', 'var2', 'var3', 'var4', 'var5', 'var6', 'var7');
    </h2>
</article>
<?php svd('var1', 'var2', 'var3', 'var4', 'var5', 'var6', 'var7'); ?>

<fieldset>
    <p>N'accepte pas plus de 2 paramètres, or, là, il y en a 7...<br>
        Donc, dans notre exemple, ne renvoie absolument rien :-( !</p>
    <legend style="border:none; background-color:rgba(0,0,0,0);">
        <h2 <?php echo $style;
?>id="print_r">print_r($var1, $var2, $var3, $var4, $var5, $var6,
            $var7);
        </h2>
    </legend>
</fieldset>

<article class="pt-3 mt-3" id="pr">
    <h2 <?php echo $style;
?>>pr($var1, $var2, $var3, $var4, $var5, $var6, $var7);
    </h2>
</article>
<?php pr($var1, $var2, $var3, $var4, $var5, $var6, $var7); ?>


<article class="pt-3 mt-3" id="spr">
    <h2 <?php echo $style;
?>>spr('var1', 'var2', 'var3', 'var4', 'var5', 'var6', 'var7');
    </h2>
</article>
<?php spr('var1', 'var2', 'var3', 'var4', 'var5', 'var6', 'var7'); ?>