<?php

/*
 * (c) Manudon - 2019
 */

$classe = [
    [
        'nom'    => 'stroumph',
        'prenom' => 'stella',
        'notes'  => [5, 10, 15, 17, 18.5, 19, 20],
    ],
    [
        'nom'    => 'darck',
        'prenom' => 'sidous',
        'notes'  => [5, 3, 8, 4, 9.5, 1, 0.9],
    ],
];

echo ucfirst($classe[0]['nom']).' '.strtoupper($classe[0]['prenom']).' a eu comme première note '.$classe[0]['notes'][0]."\n";