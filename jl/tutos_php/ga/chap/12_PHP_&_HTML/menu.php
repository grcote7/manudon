<?php
function ajoutLien(string $page = '', string $lien='A completter',string $class='nav-link' ): string {


  return $nouveauLien= <<< monLien
  <li class="nav-item active">
  <a class="nav-link" href="$page">$lien <span class="sr-only">(current)</span></a>
  </li>
monLien ;
  return $nouveauLien ;
}
?>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Fixed navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

      <ul class="navbar-nav mr-auto">

        <?= ajoutLien('index.php', 'Acceuil' ) ; ?>
        <?= ajoutLien('blog.php', 'blog' ) ; ?>

      </ul>

      <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>
    </div>
  </nav>