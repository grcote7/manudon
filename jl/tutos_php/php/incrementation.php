<?php

/*
 * (c) Manudon - 2019
 */

$x = 1;

echo $x++.'<br />'; // Aff x (1) et lui ajoute 1

echo ++$x.'<br />'; // Ajoute 1 à x (Qui valait 2 maintenant et l'aff) => 3

echo $x--.'<br />'; // Idem en décrémentant: Là, aff valeur actuelle (3) et enlève 1 (x = 2 en fin de ligne)

echo --$x.'<br />'; // Enlève 1 (Soit 2-1 = 1) et aff (Donc, 1)