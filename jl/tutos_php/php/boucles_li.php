<?php

/*
 * (c) Manudon - 2019
 */

static $resultat = 0;

$cinq = 5 ** 7;
echo $cinq.' <==> '.pow(5, 7).'<hr>';

for ($i = 0; $i <= 15; ++$i) {
    $resultat = $i ** $i;
    echo $i.'=$i '.$resultat.'<br>';
}

echo '<hr>';

$i        = 0;
$compteur = 0;

while ($compteur < 2) {
    ++$compteur;
    echo '<h3>Chapitre '.$compteur.'</h3>';
    do {
        if ($compteur % 2 - 1) {
            ++$i; // Comme en plus de l'origine, cela fera compter de 2 en 2...
        }
        echo 'Nous en sommes a la lignes '.(++$i).'<br>';
    } while ($i < 5);
    $i = 0;
}

echo '<hr>';

$textHTML = <<<EOT
Texte en html<br>
Pas bien intérésante<br>
Seulement, il y a aussi ${compteur} !!!<br>
Impréssionnant, non ?  
EOT;

echo $textHTML;