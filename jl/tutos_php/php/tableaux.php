<?php

/*
 * (c) Manudon - 2019
 */

$multidimentionnel = [
    ['écran', 'clavier', 'souris', 'bureau', 'bouteille'],
    ['fourchette', 'cuillère', 'petite', 'grosse', 'couteaux', 'opinel'],
    ['poêle', 'crêpière', 'casserole', 'passoire', 'saladier'],
    ['noire', 'croche', 'dièse'],
];

echo '<h3>Affichage d\'un tableau multidimentionnel</h3><pre>';
for ($i = 0; $i < 4; ++$i) {
    echo  '<h4>Sous-tableau # '.$i.':</h4>';
    for ($j = 0; $j < 6; ++$j) {
        echo  '* '.$multidimentionnel[$i][$j].' ';
    }
}
echo '</pre><hr>';

/*
Bonne formule pour afficher un tableau multidimentionnel, mais pb: Cela oblige de connaître le nombre de valeurs du tableau et si on ajoute comme moi, un sous-tableau pour des mots liés à la musique, il faut alors ajouter 1 à la boucle for pour $i, sinon, il ne s'affichera pas :-(

De toutes façons, on a aussi le pb avec la boucle $j (En effet, tu as mis 6, et en effet, ça affiche bien au moins tous les items (Tant que rien ne sera modifié et qu'on aura pas plus de 6 items dans un sous-tableau...)

Mais, look ! G ajouté l'affichage d'un *, etl, on voit bien que donc, quelque soit le nombre d'items, la lecture-boucle se fera 6 fois... :-(...


    2 solutions:
    - Utiliser count() évitera ces lectures inutiles, et si on ajoute des mots, ils seront lus (On voit souvent cela ds les scripts)
    - Mais si on a pas besoin, nous d'avoir cette valeur du count(), alors, le top, c'est foreach() !

Look pour le même résultat, le code !
(En sachant que modifier les tableaux ne nuira pas à l'affichage qu'on en attend)

*/

$arr = $multidimentionnel;
$i   = 0;

foreach ($arr as $sousarr) {
    echo '<h3>Sous-tableau # '.++$i.'</h3>';
    foreach ($sousarr as $item) {
        echo '* '.$item.' ';
    }
    echo '<br>';
}

// En lg 44, là, je mets les ++ avant our avoir direct le compte qui commence à 1 (plus humain)

// Et donc, on voit bien qu'il y a autant de "*" que d'items, ni +, ni - :-) !

echo '<hr>L\'item d\'index 1 du tableau d\'index 2 est: '.ucfirst($multidimentionnel[2][1]).'<hr>';

$tableau  = ['table', 'jl' => 'chaise', 'fraise' => 'cannapé', 'meuble', 'tabouret'];
$tableau2 = ['table', 'chaise', 'cannapé', ['fraise' => 'meuble'], 'tabouret'];
// $tableau3 = ('table', 'chaise', 'cannapé', 'meuble', 'tabouret'); // FAUX !
$count = count($tableau2); var_dump($count);
for ($i = 0; $i < $count; ++$i) {
    if (!is_array($tableau2[$i])) { // On vérifie qu'on va bien n'afficher que des variables scalaires
        echo $tableau2[$i].'<br>'; // Il ne peut donc pas sortir la fraise...
    }
}
// Oui voilà, c'est ce type de solution qu'on peut aussi appliquer (Et que l'on voit énormément), mais le foreach reste le + efficace en tous point de vues :-)

echo '<pre>'; // Très bien 1/2
print_r($tableau);
print_r($tableau2);
// print_r($tableau3); => Non défini, du coup !
echo '</pre>'; // Très bien 2/2 :-)