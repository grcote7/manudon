<link rel="stylesheet" href="../../assets/css/style.css">

<?php

/*
 * (c) Manudon - 2019
 */

include_once $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.
('manudon.com' === $_SERVER['HTTP_HOST'] ?
'' : '..').'/gc7/tools/vd.php';

$var = 777;

// Avec "", les variables sont interprêtées => Donc, instruction plus lente, à éviter, et réservée pour le HTML (Class, Id, etc...)
echo "Résultat de ${var} envoyé par echo<br>\n";

// Utilisée principalement pour afficher des var de array (Voir ci-dessous)
print_r("Résultat de ${var} envoyé par print_r<br>\n");
echo 'Autre résultat de $var envoyé par echo<br>';

// Un commentaire sur une seul ligne

/*
un autre sur
plusieurs

les commentaires avec # sont plus pour d'autres langages
*/

$arr = [1, 2, 3];

print_r($arr); // Peu lisible

echo '<pre>';
print_r($arr); // Très lisible
echo '</pre>';

$arr2 = [
    'Man1 en plein dans MySQL et Bootstrap' => 'MO',
    'Man2 en plein dans le PHP et la POO'   => 'JL',
    'Man3 un peu partout'                   => 'GC7',
];

print_r($arr2); // Pas du tout lisible

echo '<pre>';
print_r($arr2); // Très lisible
echo '</pre>';

echo 'Nous sommes dans un fichier de '.$arr2['Man2 en plein dans le PHP et la POO'].'.<br>';

// Il existe des tas de fonctions sur les array ainsi construits. Exemple :

$arr3 = $arr2; // Copie le tableau $arr2 dans une autre varaible, car certaines fonctions modifie le varaible d'origine
sort($arr3);
echo '<pre>';
print_r($arr3); // Sort par ordre alphabétique les valeurs (Perte des keys)
echo '</pre>';

array_flip($arr2);
echo '<pre>';
print_r($arr2); // Remplace les clés par les valeurs et inversement
echo '</pre><hr>';

vd($arr2);

echo 'Voir <a href="https://www.php.net/manual/fr/ref.array.php" target="_blank">toutes les fonctions pour les array</a>';