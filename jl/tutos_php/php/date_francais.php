<?php

/*
 * (c) Manudon - 2019
 */

function dateFrancais()
{
    /**
     * Place la date en francais dans un tableau.
     *
     * @var['j'] jour + num / mois / année
     * @var['m'] mois
     * @var['a'] année.
     *
     * @author JLB
     */
    $jour = [
        '',
        'Lundi',
        'Mardi',
        'Mercredi',
        'Jeudi',
        'Vendredi',
        'Samedi',
        'Dimanche',
    ];
    $mois = [
        '',
        'Janvier',
        'Février',
        'Mars',
        'Avril',
        'Mai',
        'Juin',
        'Juillet',
        'Août',
        'Septembre',
        'Octobre',
        'Novembre',
        'Décembre',
    ];

    return ['j' => $jour[date('N')].' '.date('d'), 'm' => $mois[date('n')], 'a' => date('Y')];
}

$dateJ = dateFrancais();
// var_dump($dateJ);
echo $dateJ['j'].' '.$dateJ['m'].' '.$dateJ['a'];