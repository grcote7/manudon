<canvas id="particle-canvas"></canvas>
<script src="particle.js"></script>

<?php

/**
 * Retourne le code HTML pour l'affichage dans une liste groupée selon bootstrap.
 *
 * On appelle ce type de fonction un helper
 *
 * @param string $sujet
 * @param string $lien
 */
function lk($sujet, $lien)
{
    return '<li class="list-group-item p-1"><a class="card-link" href="'.$lien.'">'.$sujet.'</a>
    </li>';
}
?>

<ul class="list-group my-4">
    <?php echo lk('Comparer echo et print', 'php/comparatif_echo.php'); ?>
    <?php echo lk('Incrémentation', 'php/incrementation.php'); ?>
</ul>

<ul class="list-group my-3">
    <?php echo lk('Var_dump()', 'php/var_dump.php'); ?>
    <?php echo lk('Tableaux', 'php/tableaux.php'); ?>
</ul>

<ul class="list-group my-3">
    <?php echo lk('Conditions', 'php/conditions.php'); ?>
    <?php echo lk('Les Boucles', 'php/boucles.php'); ?>
    <?php // lk('Les Boucles (Version Li)', 'PHP/boucles_li.php');
     ?>
    <?php echo lk('Les Constantes', 'php/constante.php'); ?>
    <?php echo lk('Les fonctions', 'php/fonctions.php'); ?>
    <?php echo lk('Les super global', 'php/super_global.php'); ?>
</ul>

<ul class="list-group my-3">
    <?php echo lk('TIMESTAMP', 'php/timestamp.php'); ?>
    <?php echo lk('Formater une date en PHP', 'php/formater_date.php'); ?>
    <?php echo lk('Date en Français', 'php/date_francais.php'); ?>
</ul>

<ul class="list-group my-3">
    <?php echo lk('Regex', 'php/regex.php'); ?>
</ul>