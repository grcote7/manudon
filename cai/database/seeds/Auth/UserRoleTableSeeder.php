<?php

/*
 * (c) Manudon - 2019
 */

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole(config('access.users.admin_role')); // GC7
        User::find(2)->assignRole('executive');
        User::find(3)->assignRole(config('access.users.default_role'));

        User::find(4)->assignRole(config('access.users.admin_role')); // JL
        User::find(5)->assignRole(config('access.users.admin_role')); // MO
        User::find(6)->assignRole(config('access.users.admin_role')); // ROM

        $this->enableForeignKeys();
    }
}