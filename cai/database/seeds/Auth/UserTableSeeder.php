<?php

/*
 * (c) Manudon - 2019
 */

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([ // GC7
            'first_name'        => 'Lionel',
            'last_name'         => 'CÔTE',
            'email'             => 'GC7@ManuDon.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        User::create([ // Executif
            'first_name'        => 'Backend',
            'last_name'         => 'User',
            'email'             => 'exec@ManuDon.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        User::create([ // User
            'first_name'        => 'Default',
            'last_name'         => 'User',
            'email'             => 'user@ManuDon.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        User::create([ // JL
            'first_name'        => 'Jean-Luc',
            'last_name'         => 'BIALLE',
            'email'             => 'JL@ManuDon.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        User::create([ // MO
            'first_name'        => 'Mohamed',
            'last_name'         => 'THIOUBADOU',
            'email'             => 'MO@ManuDon.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        User::create([ // ROM
            'first_name'        => 'Romain',
            'last_name'         => 'JOOSKENS',
            'email'             => 'ROM@ManuDon.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
        ]);

        $this->enableForeignKeys();
    }
}
