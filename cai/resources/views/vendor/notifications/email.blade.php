@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
    # {{ $greeting }}
@else
    @if ($level === 'error')
        # @lang('strings.emails.auth.error')
    @else
        # @lang('strings.emails.auth.greeting')
    @endif
@endif

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}
@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('strings.emails.auth.regards')<br>
L'équipe {{ config('app.name') }}.
@endif

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')

@lang(
    'strings.emails.auth.trouble_clicking_button',
    [
        'action_text' => $actionText
    ]
)

[{{ $actionUrl }}]({{ $actionUrl }})

@endslot
@endisset
@endcomponent
