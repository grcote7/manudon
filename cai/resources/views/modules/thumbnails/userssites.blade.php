@php
include_once '../resources/views/modules/tools/path.php';
@endphp

<template>
    <div class="card">
        <div class="card-header">
            <i class="fas fa-desktop"></i> Pages des membres
        </div>

        <div class="card-body">
            @if (!$sg)
            <div class="row">
                <div class="col">
                    <div class="alert alert-info">
                        <p class="text-justify small my-0">
                            NB: Vous consultez cette page en local... Le lien des sites des membres ci-dessous est donc
                            également destiné à un usage local pour dev (En utilisant des VirtualHosts)...<br>
                            Si c'est juste pour consulter l'un de ces sites, veuillez de préférence utiliser les liens
                            du site officiel <a href="http://ManuDon.com" target="_blank">
                                ManuDon.com</a>
                        </p>
                    </div>
                </div>
            </div>
            @endif

            <div class="row">
                <?php
        $sites = ['jl', 'mo', 'rom', 'gc7'];
        ?>
                @foreach ($sites as $site)
                <div class="col-6 col-3-md p-2 text-center">
                    <?php $url = ($sg) ? ('https://manudon.com/'.$site) : 'http://'.$site; ?>

                    <h4>
                        <!-- <a class="text-decoration-none" href="{{ $url }}" target="_blank">Site de
                        <b class="text-uppercase">{{ $site }} <i class="fa fa-external-link"
                                aria-hidden="true"></i></b></a> -->
                        <a class="text-decoration-none" href="{{ $url }}">Site de
                            <b class="text-uppercase">{{ $site }}</b></a>
                    </h4>
                    <div class="embed-responsive embed-responsive-1by1 w-100" style="max-height: 270px">
                        <iframe class="embed-responsive-item" src="<?php echo 'https://manudon.com/'.$site; ?>"
                            height="100%"></iframe>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</template>