    <div class="card">
        <div class="card-header">
            <i class="fas fa-code"></i> Tutos
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col">
                    <a class="text-decoration-none" href="https://c57.fr/doc/outils/tutos" target="_blank">Liste de
                        c57.fr <i aria-hidden="true" class="fa fa-external-link"></i></a>
                </div>

                <div class="col">
                    <a class="text-decoration-none" href="http://manudon.com/tutos/bootstrap/index.php">Boostrap</a>
                </div>
            </div>
        </div>

    </div>