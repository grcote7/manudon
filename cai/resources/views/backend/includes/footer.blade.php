<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }}
            <a href="/">
                @lang('strings.backend.general.manudon_link')
            </a>
        </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

    <div class="ml-auto">Theme by <a href="http://c57.fr" target="_blank">c57</a></div>
</footer>
