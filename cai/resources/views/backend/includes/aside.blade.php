<aside class="aside-menu">
    <div class="row m-3">
        <div class="h1">
            Rôle de ce menu ?
        </div>
        <div class="text-center">
            <hr />
            N'hésitez pas à en débattre dans
            <div class="btn btn-primary mt-2">
                <a
                    href="https://gitlab.com/grcote7/manudon/issues"
                    class="text-light"
                    target="_blank"
                    >Issues</a
                >
            </div>
        </div>
    </div>
</aside>
