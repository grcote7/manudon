<?php

/*
 * (c) Manudon - 2019
 */

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

/**
 * Class VerifyCsrfToken.
 */
class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        // Pour désactiver csrf (Heure client doit être OK pour éviter Err 419)
        // 'login',
    ];
}